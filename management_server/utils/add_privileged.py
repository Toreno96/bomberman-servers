import getpass
import os
import sys

import pymongo

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))
import management_server

username = input('Username: ')
password = getpass.getpass()

new_privileged = {
    'username': username,
    'password': management_server._hash_password(password),
}
with pymongo.MongoClient() as db:
    db.bomberman.privileged.insert_one(new_privileged)
