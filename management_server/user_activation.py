import json

import cherrypy
import requests

CREDENTIALS = {
    'username': '',
    'password': '',
}

AUTH = requests.auth.HTTPBasicAuth(
    CREDENTIALS['username'],
    CREDENTIALS['password']
)

class UserActivation:
    def __init__(self, auth):
        self.auth = auth

    @cherrypy.expose
    def activate(self, nickname, activation_key):
        url = 'http://localhost:8080/api/privileged/users/'
        response = requests.get(url, auth=self.auth, params={'nickname': nickname})
        if response.ok:
            found_user = json.loads(response.content.decode())
            if found_user[nickname]['activated']:
                return 'Your account has already been activated'
            response = requests.patch(url, auth=self.auth, data={'nickname': nickname, 'activation_key': activation_key})
            if response.ok:
                return 'Your account has been activated'
        return '{} {}'.format(response.status_code, response.reason)

def main():
    cherrypy.config.update({'server.socket_port': 9090})
    cherrypy.quickstart(UserActivation(AUTH), '/')

if __name__ == '__main__':
    main()
