import json

import requests

# Prepare database environment
import users_service

# It assumes that there is such privileged user in the database
AUTH = requests.auth.HTTPBasicAuth('debug', 'secret')

# Get statistics for all players
URL = 'http://localhost:8080/api/privileged/ranking/'
response = requests.get(URL, auth=AUTH)
if response.ok:
    statistics = json.loads(response.content.decode())
    print(json.dumps(statistics, indent=4))
    print()

# Get scores for all players (scores parameter defaults to 'false'; for all
# valid values see distutils.util.strtobool)
response = requests.get(URL, auth=AUTH, params={'scores': 'true'})
if response.ok:
    statistics = json.loads(response.content.decode())
    print(json.dumps(statistics, indent=4))
    print()

# Get statistics for specific player
response = requests.get(URL, auth=AUTH, params={'nickname': 'Alice', 'scores': 'true'})
if response.ok:
    statistics = json.loads(response.content.decode())
    print(json.dumps(statistics, indent=4))
    print()


# Add statistics for new game
PLAYERS_COUNT = 3
new_statistics = {
    'Alice': {
        'players_count': PLAYERS_COUNT,
        'place': 1,
    },
    'Bob': {
        'players_count': PLAYERS_COUNT,
        'place': 2,
    },
    'Charlie': {
        'players_count': PLAYERS_COUNT,
        'place': 3,
    },
}
response = requests.post(URL, auth=AUTH, json=new_statistics)
if response.ok:
    print('statistics added for players: {}'.format(', '.join(new_statistics.keys())))
    print()
