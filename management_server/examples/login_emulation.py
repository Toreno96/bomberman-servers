import getpass

import requests

nickname = input('Nickname: ')
password = getpass.getpass()

AUTH = requests.auth.HTTPBasicAuth(nickname, password)
URL = 'http://localhost:8080/api/users/'
response = requests.get(URL, auth=AUTH)
if response.ok:
    print('Request successful; cached credentials can be reused in the future requests')
elif response.status_code == 401:
    print('Invalid username or password (for details parse response.text)')
elif response.status_code == 403:
    print('Account not activated')
else:
    print('Error: {} {}'.format(response.status_code, response.reason))
