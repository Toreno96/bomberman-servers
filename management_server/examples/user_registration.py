import getpass

import requests

nickname = input('Nickname: ')
email = input('E-Mail: ')
password = getpass.getpass()

new_player = {
    'nickname': nickname,
    'email': email,
    'password': password,
}
URL = 'http://localhost:8080/api/users/'
response = requests.post(URL, json=[new_player])
if response.ok:
    print('Registration successful')
elif response.status_code == 409:
    print('Player with such nickname or email already exists')
else:
    print('Error: {} {}'.format(response.status_code, response.reason))
