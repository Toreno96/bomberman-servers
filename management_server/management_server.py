import base64
import distutils
import distutils.util
import hashlib
import os

import cherrypy
import pymongo

import email_sender

def _hash_password(password):
    return hashlib.sha256(password.encode()).hexdigest()

def _generate_activation_key():
    return base64.urlsafe_b64encode(os.urandom(256)).decode()

def _validate_privileged_password(realm, username, password):
    with pymongo.MongoClient() as db:
        privileged = db.bomberman.privileged.find({})
        credentials = {p['username']: p['password'] for p in privileged}
        if username not in credentials:
            raise cherrypy.HTTPError(status=401, message='Invalid username')
        elif credentials[username] != _hash_password(password):
            raise cherrypy.HTTPError(status=401, message='Invalid password')
        else:
            return True

def _validate_player_password(realm, username, password):
    with pymongo.MongoClient() as db:
        players = [p for p in db.bomberman.players.find()]
        credentials = {p['nickname']: p['password'] for p in players}
        activated = [p['nickname'] for p in players if p['activated'] == True]
        if username not in credentials:
            raise cherrypy.HTTPError(status=401, message='Invalid username')
        elif credentials[username] != _hash_password(password):
            raise cherrypy.HTTPError(status=401, message='Invalid password')
        elif username not in activated:
            raise cherrypy.HTTPError(status=403, message='Account not activated')
        else:
            return True

@cherrypy.expose
class UsersService:
    @cherrypy.tools.json_out()
    def GET(self, nickname=None):
        with pymongo.MongoClient() as db:
            if nickname:
                players = db.bomberman.players.find({'nickname': nickname})
            else:
                players = db.bomberman.players.find({})
            if not players.count():
                raise cherrypy.HTTPError(status=404)
            keys = ['activated']
            return {p['nickname']: {k: p[k] for k in keys} for p in players}

    @cherrypy.tools.json_in()
    # Could be validated like PATCH, if user registration would be implemented
    # via webapp, too.
    @cherrypy.tools.auth_basic(on=False)
    def POST(self):
        data = cherrypy.request.json
        with pymongo.MongoClient() as db:
            players = [p for p in db.bomberman.players.find({})]
            nicknames = {p['nickname'].casefold() for p in players}
            emails = {p['email'].casefold() for p in players}
            if nicknames.intersection({d['nickname'].casefold() for d in data}) \
                    or emails.intersection({d['email'].casefold() for d in data}):
                raise cherrypy.HTTPError(409)
            else:
                new_players = [{'nickname': d['nickname'],
                                'email': d['email'],
                                'password': _hash_password(d['password']),
                                'activation_key': _generate_activation_key(),
                                'activated': False,
                                'statistics_per_game': []} for d in data]
                if new_players:
                    db.bomberman.players.insert(new_players)
                for player in new_players:
                    email_sender.send_account_activation_mail(player)
                cherrypy.response.status = 201

@cherrypy.expose
class UsersPrivilegedService(UsersService):
    def PATCH(self, nickname, activation_key):
        with pymongo.MongoClient() as db:
            if db.bomberman.players.find({'nickname': nickname, 'activation_key': activation_key}).count() == 1:
                db.bomberman.players.update({'nickname': nickname}, {'$set': {'activated': True}}, upsert=False)
            else:
                raise cherrypy.HTTPError(404)

def _has_required_statistics(statistics):
    required_statistics = {'players_count', 'place'}
    return {k for k in statistics.keys()}.issuperset(required_statistics)

def _calculate_single_game_score(statistics):
    return int(statistics['players_count'] ** 2 / statistics['place'])

def _calculate_all_games_score(statistics):
    return sum([_calculate_single_game_score(s) for s in statistics])

@cherrypy.expose
class RankingService:
    @cherrypy.tools.json_out()
    def GET(self, nickname=None, scores='false'):
        with pymongo.MongoClient() as db:
            if nickname:
                players = db.bomberman.players.find({'nickname': nickname})
            else:
                players = db.bomberman.players.find({})
            if not players.count():
                raise cherrypy.HTTPError(status=404)
            ranking = {p['nickname']: p['statistics_per_game'] for p in players}
            try:
                if distutils.util.strtobool(scores):
                    ranking = {k: _calculate_all_games_score(v) for k, v in ranking.items()}
            except ValueError:
                raise cherrypy.HTTPError(status=400)
            return ranking

@cherrypy.expose
class RankingPrivilegedService(RankingService):
    @cherrypy.tools.json_in()
    def POST(self):
        data = cherrypy.request.json
        with pymongo.MongoClient() as db:
            players = {p['nickname'] for p in db.bomberman.players.find({})}
            if not {k for k in data.keys()}.difference(players):
                for k, v in data.items():
                    if isinstance(v, dict) and _has_required_statistics(v):
                        db.bomberman.players.update({'nickname': k}, {'$push': {'statistics_per_game': v}}, upsert=False)
                    else:
                        raise cherrypy.HTTPError(400)
                cherrypy.response.status = 201
            else:
                raise cherrypy.HTTPError(404)

@cherrypy.expose
class GameServersService:
    @cherrypy.tools.json_out()
    def GET(self, name=None):
        with pymongo.MongoClient() as db:
            if name:
                game_servers = db.bomberman.game_servers.find({'name': name})
            else:
                game_servers = db.bomberman.game_servers.find({})
            if not game_servers.count():
                raise cherrypy.HTTPError(status=404)
            keys = ['ip', 'port', 'max_players_count', 'game_in_progress']
            return {g['name']: {k: g[k] for k in keys} for g in game_servers}

@cherrypy.expose
class GameServersPrivilegedService(GameServersService):
    @cherrypy.tools.json_in()
    def POST(self):
        data = cherrypy.request.json
        with pymongo.MongoClient() as db:
            game_servers = [p for p in db.bomberman.game_servers.find({})]
            names = {p['name'].casefold() for p in game_servers}
            if names.intersection({d['name'].casefold() for d in data}):
                raise cherrypy.HTTPError(409)
            else:
                new_game_servers = [{'name': d['name'],
                                     'ip': d['ip'],
                                     'port': d['port'],
                                     'max_players_count': d['max_players_count'],
                                     'game_in_progress': False} for d in data]
                if new_game_servers:
                    db.bomberman.game_servers.insert(new_game_servers)
                cherrypy.response.status = 201

    def PATCH(self, name, game_in_progress):
        with pymongo.MongoClient() as db:
            if db.bomberman.game_servers.find({'name': name}).count() == 1:
                try:
                    db.bomberman.game_servers.update({'name': name}, {'$set': {'game_in_progress': bool(distutils.util.strtobool(game_in_progress))}}, upsert=False)
                except ValueError:
                    raise cherrypy.HTTPError(status=400)
            else:
                raise cherrypy.HTTPError(404)

    def DELETE(self, name):
        with pymongo.MongoClient() as db:
            result = db.bomberman.game_servers.delete_one({'name': name})
            if result.deleted_count == 0:
                raise cherrypy.HTTPError(status=404)
            else:
                cherrypy.response.status = 200

def main():
    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'request.show_tracebacks': None,
            'tools.auth_basic.on': True,
            'tools.auth_basic.realm': 'localhost',
            'tools.auth_basic.checkpassword': _validate_player_password,
            'tools.auth_basic.accept_charset': 'UTF-8',
        }
    }
    privileged_conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'request.show_tracebacks': None,
            'tools.auth_basic.on': True,
            'tools.auth_basic.realm': 'localhost',
            'tools.auth_basic.checkpassword': _validate_privileged_password,
            'tools.auth_basic.accept_charset': 'UTF-8',
        }
    }
    cherrypy.tree.mount(UsersService(), '/api/users/', conf)
    cherrypy.tree.mount(GameServersService(), '/api/game_servers/', conf)
    cherrypy.tree.mount(RankingService(), '/api/ranking/', conf)
    cherrypy.tree.mount(UsersPrivilegedService(), '/api/privileged/users/', privileged_conf)
    cherrypy.tree.mount(GameServersPrivilegedService(), '/api/privileged/game_servers/', privileged_conf)
    cherrypy.quickstart(RankingPrivilegedService(), '/api/privileged/ranking/', privileged_conf)

if __name__ == '__main__':
    main()
